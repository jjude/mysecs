import System
from System.Windows.Forms import *
from System.ComponentModel import *
from System.Drawing import *
from clr import *


import SourceGrid
import mySecsHelper
import googleHelper
import ZedGraph
import zedgraphHelper
class mysecs20: # namespace
    
    class MainForm(System.Windows.Forms.Form):
        """type(_notifyIcon) == System.Windows.Forms.NotifyIcon, type(_tabControl) == System.Windows.Forms.TabControl, type(_menuStrip) == System.Windows.Forms.MenuStrip, type(_statusStrip) == System.Windows.Forms.StatusStrip, type(_toolStripStatusLabel) == System.Windows.Forms.ToolStripStatusLabel, type(_programToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_refreshToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_downloadToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_toolStripSeparator1) == System.Windows.Forms.ToolStripSeparator, type(_exitToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_helpToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_aboutToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_DataGrid) == SourceGrid.Grid, type(_components) == System.ComponentModel.IContainer, type(_DashboardTabPage) == System.Windows.Forms.TabPage, type(_zedGraphControl) == ZedGraph.ZedGraphControl, type(_refreshGraphToolStripMenuItem) == System.Windows.Forms.ToolStripMenuItem, type(_DataTabPage) == System.Windows.Forms.TabPage"""
        __slots__ = ['_notifyIcon', '_tabControl', '_menuStrip', '_statusStrip', '_toolStripStatusLabel', '_programToolStripMenuItem', '_refreshToolStripMenuItem', '_downloadToolStripMenuItem', '_toolStripSeparator1', '_exitToolStripMenuItem', '_helpToolStripMenuItem', '_aboutToolStripMenuItem', '_DataGrid', '_components', '_DashboardTabPage', '_zedGraphControl', '_refreshGraphToolStripMenuItem', '_DataTabPage']
        def __init__(self):
            self._InitializeComponent()
            self.initNotifyIcon()
            self.Resize += self.ResizeForm
        
        @accepts(Self())
        @returns(None)
        def _InitializeComponent(self):
            self._components = System.ComponentModel.Container()
            self._menuStrip = System.Windows.Forms.MenuStrip()
            self._programToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._refreshToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._downloadToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._toolStripSeparator1 = System.Windows.Forms.ToolStripSeparator()
            self._exitToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._helpToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._aboutToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._statusStrip = System.Windows.Forms.StatusStrip()
            self._toolStripStatusLabel = System.Windows.Forms.ToolStripStatusLabel()
            self._tabControl = System.Windows.Forms.TabControl()
            self._DashboardTabPage = System.Windows.Forms.TabPage()
            self._zedGraphControl = ZedGraph.ZedGraphControl()
            self._DataTabPage = System.Windows.Forms.TabPage()
            self._DataGrid = SourceGrid.Grid()
            self._notifyIcon = System.Windows.Forms.NotifyIcon(self._components)
            self._refreshGraphToolStripMenuItem = System.Windows.Forms.ToolStripMenuItem()
            self._menuStrip.SuspendLayout()
            self._statusStrip.SuspendLayout()
            self._tabControl.SuspendLayout()
            self._DashboardTabPage.SuspendLayout()
            self._DataTabPage.SuspendLayout()
            self.SuspendLayout()
            # 
            # menuStrip
            # 
            self._menuStrip.Items.AddRange(System.Array[System.Windows.Forms.ToolStripItem]((self._programToolStripMenuItem, self._helpToolStripMenuItem, )))
            self._menuStrip.Location = System.Drawing.Point(0, 0)
            self._menuStrip.Name = 'menuStrip'
            self._menuStrip.Size = System.Drawing.Size(746, 24)
            self._menuStrip.TabIndex = 0
            self._menuStrip.Text = 'menuStrip1'
            # 
            # programToolStripMenuItem
            # 
            self._programToolStripMenuItem.DropDownItems.AddRange(System.Array[System.Windows.Forms.ToolStripItem]((self._refreshGraphToolStripMenuItem, self._refreshToolStripMenuItem, self._downloadToolStripMenuItem, self._toolStripSeparator1, self._exitToolStripMenuItem, )))
            self._programToolStripMenuItem.Name = 'programToolStripMenuItem'
            self._programToolStripMenuItem.Size = System.Drawing.Size(59, 20)
            self._programToolStripMenuItem.Text = '&Program'
            # 
            # refreshToolStripMenuItem
            # 
            self._refreshToolStripMenuItem.Name = 'refreshToolStripMenuItem'
            self._refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R
            self._refreshToolStripMenuItem.Size = System.Drawing.Size(197, 22)
            self._refreshToolStripMenuItem.Text = '&Refresh Data'
            self._refreshToolStripMenuItem.Click += self._refreshToolStripMenuItem_Click
            # 
            # downloadToolStripMenuItem
            # 
            self._downloadToolStripMenuItem.Name = 'downloadToolStripMenuItem'
            self._downloadToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D
            self._downloadToolStripMenuItem.Size = System.Drawing.Size(197, 22)
            self._downloadToolStripMenuItem.Text = '&Download Data'
            self._downloadToolStripMenuItem.Click += self._downloadToolStripMenuItem_Click
            # 
            # toolStripSeparator1
            # 
            self._toolStripSeparator1.Name = 'toolStripSeparator1'
            self._toolStripSeparator1.Size = System.Drawing.Size(194, 6)
            # 
            # exitToolStripMenuItem
            # 
            self._exitToolStripMenuItem.Name = 'exitToolStripMenuItem'
            self._exitToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X
            self._exitToolStripMenuItem.Size = System.Drawing.Size(197, 22)
            self._exitToolStripMenuItem.Text = '&Exit'
            self._exitToolStripMenuItem.Click += self._exitToolStripMenuItem_Click
            # 
            # helpToolStripMenuItem
            # 
            self._helpToolStripMenuItem.DropDownItems.AddRange(System.Array[System.Windows.Forms.ToolStripItem]((self._aboutToolStripMenuItem, )))
            self._helpToolStripMenuItem.Name = 'helpToolStripMenuItem'
            self._helpToolStripMenuItem.Size = System.Drawing.Size(40, 20)
            self._helpToolStripMenuItem.Text = '&Help'
            # 
            # aboutToolStripMenuItem
            # 
            self._aboutToolStripMenuItem.Name = 'aboutToolStripMenuItem'
            self._aboutToolStripMenuItem.Size = System.Drawing.Size(114, 22)
            self._aboutToolStripMenuItem.Text = '&About'
            self._aboutToolStripMenuItem.Click += self._aboutToolStripMenuItem_Click
            # 
            # statusStrip
            # 
            self._statusStrip.Items.AddRange(System.Array[System.Windows.Forms.ToolStripItem]((self._toolStripStatusLabel, )))
            self._statusStrip.Location = System.Drawing.Point(0, 414)
            self._statusStrip.Name = 'statusStrip'
            self._statusStrip.Size = System.Drawing.Size(746, 22)
            self._statusStrip.TabIndex = 1
            self._statusStrip.Text = 'statusStrip1'
            # 
            # toolStripStatusLabel
            # 
            self._toolStripStatusLabel.Name = 'toolStripStatusLabel'
            self._toolStripStatusLabel.Size = System.Drawing.Size(0, 17)
            # 
            # tabControl
            # 
            self._tabControl.Controls.Add(self._DashboardTabPage)
            self._tabControl.Controls.Add(self._DataTabPage)
            self._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
            self._tabControl.Location = System.Drawing.Point(0, 24)
            self._tabControl.Name = 'tabControl'
            self._tabControl.SelectedIndex = 0
            self._tabControl.Size = System.Drawing.Size(746, 390)
            self._tabControl.TabIndex = 3
            # 
            # DashboardTabPage
            # 
            self._DashboardTabPage.Controls.Add(self._zedGraphControl)
            self._DashboardTabPage.Location = System.Drawing.Point(4, 22)
            self._DashboardTabPage.Name = 'DashboardTabPage'
            self._DashboardTabPage.Padding = System.Windows.Forms.Padding(3)
            self._DashboardTabPage.Size = System.Drawing.Size(738, 364)
            self._DashboardTabPage.TabIndex = 1
            self._DashboardTabPage.Text = 'Dashboard'
            self._DashboardTabPage.UseVisualStyleBackColor = True
            # 
            # zedGraphControl
            # 
            self._zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill
            self._zedGraphControl.Location = System.Drawing.Point(3, 3)
            self._zedGraphControl.Name = 'zedGraphControl'
            self._zedGraphControl.ScrollGrace = 0.0
            self._zedGraphControl.ScrollMaxX = 0.0
            self._zedGraphControl.ScrollMaxY = 0.0
            self._zedGraphControl.ScrollMaxY2 = 0.0
            self._zedGraphControl.ScrollMinX = 0.0
            self._zedGraphControl.ScrollMinY = 0.0
            self._zedGraphControl.ScrollMinY2 = 0.0
            self._zedGraphControl.Size = System.Drawing.Size(732, 358)
            self._zedGraphControl.TabIndex = 0
            # 
            # DataTabPage
            # 
            self._DataTabPage.Controls.Add(self._DataGrid)
            self._DataTabPage.Location = System.Drawing.Point(4, 22)
            self._DataTabPage.Name = 'DataTabPage'
            self._DataTabPage.Padding = System.Windows.Forms.Padding(3)
            self._DataTabPage.Size = System.Drawing.Size(738, 364)
            self._DataTabPage.TabIndex = 0
            self._DataTabPage.Text = 'Data'
            self._DataTabPage.UseVisualStyleBackColor = True
            # 
            # DataGrid
            # 
            self._DataGrid.AutoStretchColumnsToFitWidth = True
            self._DataGrid.ColumnsCount = 5
            self._DataGrid.Dock = System.Windows.Forms.DockStyle.Fill
            self._DataGrid.FixedRows = 1
            self._DataGrid.Location = System.Drawing.Point(3, 3)
            self._DataGrid.Name = 'DataGrid'
            self._DataGrid.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows
            self._DataGrid.SelectionMode = SourceGrid.GridSelectionMode.Cell
            self._DataGrid.Size = System.Drawing.Size(732, 358)
            self._DataGrid.TabIndex = 0
            self._DataGrid.TabStop = True
            self._DataGrid.ToolTipText = ''
            # 
            # refreshGraphToolStripMenuItem
            # 
            self._refreshGraphToolStripMenuItem.Name = 'refreshGraphToolStripMenuItem'
            self._refreshGraphToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G
            self._refreshGraphToolStripMenuItem.Size = System.Drawing.Size(197, 22)
            self._refreshGraphToolStripMenuItem.Text = 'Refresh &Graph'
            self._refreshGraphToolStripMenuItem.Click += self._refreshGraphToolStripMenuItem_Click
            # 
            # MainForm
            # 
            self.ClientSize = System.Drawing.Size(746, 436)
            self.Controls.Add(self._tabControl)
            self.Controls.Add(self._statusStrip)
            self.Controls.Add(self._menuStrip)
            self.MainMenuStrip = self._menuStrip
            self.Name = 'MainForm'
            self.Text = 'mySecs'
            self.Load += self._MainForm_Load
            self._menuStrip.ResumeLayout(False)
            self._menuStrip.PerformLayout()
            self._statusStrip.ResumeLayout(False)
            self._statusStrip.PerformLayout()
            self._tabControl.ResumeLayout(False)
            self._DashboardTabPage.ResumeLayout(False)
            self._DataTabPage.ResumeLayout(False)
            self.ResumeLayout(False)
            self.PerformLayout()
        
        @accepts(Self(), bool)
        @returns(None)
        def Dispose(self, disposing):
            super(type(self), self).Dispose(disposing)
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _exitToolStripMenuItem_Click(self, sender, e):
            Application.Exit()
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _MainForm_Load(self, sender, e):
            zedgraphHelper.DrawGraphs(self)
            mySecsHelper.RefreshGrid(self)
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _refreshToolStripMenuItem_Click(self, sender, e):
            mySecsHelper.RefreshGrid(self)
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _downloadToolStripMenuItem_Click(self, sender, e):
            mySecsHelper.DownloadToCSV(self)
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _aboutToolStripMenuItem_Click(self, sender, e):
            MessageBox.Show('mySecs is a time tracking tool\nFor more details refer http://www.jjude.com', 'About mySecs')
        
        @accepts(Self())
        @returns(None)
        def initNotifyIcon(self):
            self._notifyIcon.Icon = Icon('time.ico')
            self._notifyIcon.Visible = False
            self._notifyIcon.MouseDoubleClick += self.DoubleClickOnTrayIcon
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def ResizeForm(self, sender, e):
            if self.WindowState == FormWindowState.Minimized:
                self._notifyIcon.Visible = True
                self.Visible = False
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def DoubleClickOnTrayIcon(self, sender, e):
            if e.Button == MouseButtons.Left:
                self.Visible = True
                if self.WindowState == FormWindowState.Minimized:
                    self.WindowState = FormWindowState.Normal
                self._notifyIcon.Visible = False
        
        @accepts(Self(), System.Object, System.EventArgs)
        @returns(None)
        def _refreshGraphToolStripMenuItem_Click(self, sender, e):
            zedgraphHelper.DrawGraphs(self)            
        
    

