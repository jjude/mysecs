from System import *
from System.Windows.Forms import *
from MainForm import *
import clr
clr.AddReference('System.Xml')

import mySecsHelper
import googleHelper

from NSpring.Logging import Logger

class mysecs10: # namespace
    LogFileName = "Log\\" + DateTime.Now.ToString("yyyy-MM-dd hh.mm.ss") + "-mySecs.log";
    mysecsLog = Logger.CreateFileLogger(LogFileName, "{ts} {msg}")
    IsOnDev = 0 #not on local development environment
    donttracklist=[]
    categories={}
    
    @staticmethod
    def RealEntryPoint():        
        mysecs10.mysecsLog.Open()           
        mysecs10.mysecsLog.Log("Starting myseces")
        
        capturetimer = Timer()
        #5 secs
        capturetimer.Interval = 1000 * 5
        capturetimer.Tick += mySecsHelper.SnapshotDetails
        capturetimer.Start()
        
        appsTimer = Timer()
        #15 min
        appsTimer.Interval = 1000 * 15 * 60        
                
        #read config details
        ConfigXML = System.Xml.XmlDocument()
        ConfigXML.Load("config.xml")
        UpdateServer = int(ConfigXML.SelectSingleNode("//serverupdate").InnerText)
        Host = ConfigXML.SelectSingleNode("//host").InnerText
        EMail = ConfigXML.SelectSingleNode("//email").InnerText
        Password = ConfigXML.SelectSingleNode("//password").InnerText
        mysecs10.IsOnDev = int(ConfigXML.SelectSingleNode("//IsOnDev").InnerText)
        
        #read tracking details
        donttracknodes = ConfigXML.SelectNodes("//donttrack")
        for node in donttracknodes:
            for item in node.ChildNodes:
                mysecs10.donttracklist.append(item.InnerText.lower())
        
        mysecs10.mysecsLog.Log("dont tracking list is: %s" % mysecs10.donttracklist)
        
        #read categories
        appmaping  = ConfigXML.SelectNodes("//appmaping")
        for node in appmaping:
            for app in node.ChildNodes:
                mysecs10.categories[app.InnerText.lower()] = app.GetAttribute("category")
        
        mysecs10.mysecsLog.Log("cat dict is: %s" % mysecs10.categories)

        if UpdateServer==1:
            mysecs10.mysecsLog.Log("host:%s email:%s password:%s" %(Host, EMail, Password))                                
            googleApps = googleHelper.googleHelper(host=Host, email=EMail, password=Password)
            appsTimer.Tick += googleApps.PostData
            appsTimer.Start()
        
        Application.EnableVisualStyles()
        Application.Run(mysecs20.MainForm()) 
        
        mysecs10.mysecsLog.Log("Closing myseces")
        mysecs10.mysecsLog.Close()
        #Any running non-daemon python thread will prevent the application from exiting. 
        #Threads in python are non-daemon by default
        capturetimer.Stop()
        if appsTimer:
            appsTimer.Stop()
    
if __name__ == "Program":
    mysecs10.RealEntryPoint();    
    
    