import clr
clr.AddReference('System.Drawing')
clr.AddReference('System.Windows.Forms')
clr.AddReference('ZedGraph')

from System.Math import Sin
from System import Array
from System.Drawing import (Color, 
                            Point, 
                            Size, 
                            SizeF)
from System.Windows.Forms import (Application, 
                                  AutoScaleMode, 
                                  Form)
from ZedGraph import (Fill,
                      GraphPane,
                      LegendPos,
                      MasterPane,
                      PaneLayout,
                      Location,
                      CoordType,
                      AlignH,
                      AlignV,
                      LegendPos,
                      PieLabelType)


import mySecsHelper
import Program

def DrawGraphs(form):
    # Get a reference to the GraphPane instance in the ZedGraphControl
    myPane = form._zedGraphControl.GraphPane
    myMaster = form._zedGraphControl.MasterPane
    
    myMaster.PaneList.Clear()
    # Set the master pane title
    myMaster.Title.Text = "How did I spend my time?"
    myMaster.Title.IsVisible = True
    myMaster.Fill = Fill( Color.White, Color.MediumSlateBlue, 45.0 )
    myMaster.Margin.All = 10
    myMaster.InnerPaneGap = 10
    myMaster.IsUniformLegendEntries = True

    #Get Categories
    catPane = GraphPane()
    #set title
    catPane.Title.Text = "Top 5 Categories"
    catPane.Title.IsVisible = True
    # Set the legend to invisible
    catPane.Legend.IsVisible = False
    
    dbconn=mySecsHelper.get_or_create_db()
    dbcmd = dbconn.CreateCommand()
    #select the top 5 values grouped by category
    #timespent in hours
    dbcmd.CommandText = 'select category, round(sum(timespent/(60))) from time_details group by category order by 2 desc limit 5'
    catdetails = dbcmd.ExecuteReader()
    colors = Array[Color](( Color.Red, Color.Blue, Color.Green, Color.Yellow, Color.Aquamarine ))
    colorVal = 0
    while catdetails.Read():
        segment = catPane.AddPieSlice(catdetails[1],colors[colorVal], 0, catdetails[0])
        segment.LabelType = PieLabelType.Name_Value_Percent
        segment.LabelDetail.FontSpec.Size = 15
        colorVal = colorVal + 1
        
    myMaster.Add(catPane)    

    #Get Applications
    appPane = GraphPane()
    #set title
    appPane.Title.Text = "Top 5 Applications"
    appPane.Title.IsVisible = True
    # Set the legend to invisible
    appPane.Legend.IsVisible = False
    
    #dbconn=mySecsHelper.get_or_create_db()
    dbcmd = dbconn.CreateCommand()
    #select the top 5 values grouped by category
    #timespent in hours
    dbcmd.CommandText = 'select appname, round(sum(timespent/(60))) from time_details where appname <> "" group by appname order by 2 desc limit 5'
    appdetails = dbcmd.ExecuteReader()
    colors = Array[Color](( Color.Red, Color.Blue, Color.Green, Color.Yellow, Color.Aquamarine ))
    colorVal = 0
    while appdetails.Read():
        segment = appPane.AddPieSlice(appdetails[1],colors[colorVal], 0, appdetails[0])
        segment.LabelType = PieLabelType.Name_Value_Percent
        segment.LabelDetail.FontSpec.Size = 15
        colorVal = colorVal + 1
        
    myMaster.Add(appPane)
    dbconn.Close()
    
    # Tell ZedGraph to auto layout all the panes
    g = form._zedGraphControl.CreateGraphics()
    myMaster.SetLayout( g, PaneLayout.SquareColPreferred )

    # Tell ZedGraph to calculate the axis ranges
    form._zedGraphControl.AxisChange()
    form._zedGraphControl.Refresh()
    #SetSize(form)
    
def SetSize(form):
    form._zedGraphControl.Location = Point(10, 10)
    form._zedGraphControl.Size = Size((form.ClientRectangle.Width - 20), (form.ClientRectangle.Height - 20))
