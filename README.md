# What is mySecs? #
Set of applications (windows client, web) to measure/analyze time spent in a day. Currently only Windows Client is ready.

# Why did I create mySecs? #
There are couple of reasons.

* To start with, I wanted to learn IronPython, Python implementation for Windows .NET framework.
* I'm keen on improving productivity.
* Having these two in mind, I developed mySecs.

## How to get started? ##
1. Download the latest Windows Client Tool.
1. Unzip into a directory (do not open the zip file)
1. From the unzipped directory, execute mysecs.exe

## What about configuration? ##
There are three types of configuration in the config.xml file.

* Don't Track : List the applications that you don't want to track. It could include any games or music play or other non-productive applications. It depends on personal preference.
* Application Mapping : mySecs comes with few categorization. However it is not comprehensive. Feel free to modify and/or add to this mapping. This is of type,
<application category="Browsing">iexplore</application> Application name is as it appears in the Task Manager. It is neither the executable name nor Application Title. I know it is difficult, however as of now, I don't know of a better way. If you've any suggestions, feel free to contact me.
* Google AppEngine : I've a plan to integrate with a web application hosted in Google AppEngine. This is not fully implemented and hence can be ignored.

## Is my data safe? ##
Currently there is only a Windows Client Tool and no integration with any other web servers. Hence the data lies within the machine that you install.

## Are there any screen shots? ##
As soon as you open the tool, it will display a blank dashboard. As the tool gathers data the dashboard will be displayed properly. Dashboards contains two pie-charts. One is about top 5 applications and another is top 5 categories. Time is mentioned in minutes.

You can also download the raw data(as csv), from which you can do an analysis on your own. DataGrid screen shot: 

## How stable is mySecs? ##
It works in my machine :-). As I continue to develop, I'll fix bugs reported. If you find any bugs, please file it in the Issue Log.

## Aren't there other tools? ##
As I mentioned earlier, I started with this for learning. But as I started with the development, I came to know of few similar tools.

* Slife
* RescueTime
Go ahead and try them. If it fits your requirement, you should use them.

## How can I help? ##
* Write about it
* Do a code review
* Test and report issues
* Mention features that you would like
* Surprise me with gifts from Amazon. Check with me before you really do it. I may have already gotten the book. ;-)

## Version History ##
Oct 12, 2008 : Release of version with dashboard features

## Acknowledgment ##
* IronPython team for bringing out a simpler, easier tool for Windows GUI programming.
* IronPython Cookbook.
* Voidspace for tons of tips on IronPython.
* Zedgraph for the simple to use open source Charting library in .NET.
* DevAge for the simple to use free spreadsheet library in .NET.
* Amit, Head IS dept, iGATE for permitting to publish this as an Open Source.