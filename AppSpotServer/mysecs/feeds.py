#Implements feeds
#ref: http://www.djangoproject.com/documentation/syndication_feeds/
#ref: http://www.andrlik.org/blog/2007/aug/03/fun-with-django-feeds/
from django.contrib.syndication.feeds import Feed
from django.core.exceptions import ObjectDoesNotExist

from models import *
from django.conf import settings

class LatestPhotos(Feed):
    title = '%s : Latest Photos' % settings.SITE_NAME
    link = '/'
    description = 'Latest photos in %s' % settings.SITE_NAME

    def items(self):
        return Photo.all().order('-pub_date')[:settings.FEED_SIZE]