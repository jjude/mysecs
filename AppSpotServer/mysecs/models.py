from appengine_django.models import BaseModel
from google.appengine.ext import db
from google.appengine.ext.db import djangoforms

import logging

# models for mysecs application
class time_details(BaseModel):
    user = db.UserProperty()
    fromtime = db.DateTimeProperty(verbose_name='Start Time:')
    totime = db.DateTimeProperty(verbose_name='End Time:')
    timespent = db.IntegerProperty(verbose_name='Time Spent:')
    task = db.StringProperty(verbose_name='What did you do?:')
    taskinfo = db.StringProperty(verbose_name='More Info:')
    ipaddress = db.StringProperty(verbose_name='IP Address:')
    osversion = db.StringProperty(verbose_name='OS:')
    source = db.StringProperty(verbose_name='Update Source:')
    uploadedtime = db.DateTimeProperty(verbose_name='Uploaded Time:', auto_now_add=True)
    
    class Meta:        
        ordering = ('-uploadedtime',)
        get_latest_by = 'uploadedtime' 