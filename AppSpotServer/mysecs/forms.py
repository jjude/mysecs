from models import *

class FlickrOptionForm(djangoforms.ModelForm):
  class Meta:
    model = FlickrOption
   
class PhotoForm(djangoforms.ModelForm):
  class Meta:
    model = Photo
    #fields =('photo_id','title','body','is_published','pub_date','thumb_info','photo_info','exif_info')